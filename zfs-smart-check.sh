#! /usr/bin/env bash
# Author: Jeremy Carver
# Purpose: Check for disk failures using smartmontools

# list hard drives and output to file
disks=$(
  geom disk list | grep 'Geom name:' | sed 's/Geom name\: //' | grep -v 'mfid'
  mfiutil show drives | sed 1d | sed -e 's/^ *//' | cut -d' ' -f1 | while read x; do echo "pass${x}"; done
)
# space separated variable is fine with for loop. 
for disk in $disks; do
  echo -n $disk ' ' 
  #                              can chain these together  
  smartctl -a /dev/$disk | grep -e "SMART overall-health" -e "SMART Health"    
done
