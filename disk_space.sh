#!/bin/bash
# Author: J.Carver
# Purpose: Monitor percentage of hard drive used and report result to administrator

DISKSPACE=$(df -P / | tail -1 | awk '{print $5}' | tr -d '%')
case $DISKSPACE in
100)
echo "The disk is full"
echo "Emergency! Disk is full!" | sendemail -s mail.internode.on.net -f jeremy.carver@.com -t jeremy.carver@.com
;;
# Begins with the numbers 1 through 7 and optionally anything after,or just a single digit. This corresponds to anything under 80% full
[1-7]*|[0-9])                                                                                                              
echo "Lots of space left"
echo "Lots of space left" | sendemail -s mail.internode.on.net -f jeremy.carver@.com -t jeremy.carver@.com
;;
[8-9]*)
echo "I'm at least 80% full"
echo "I'm at least 80% full" | sendemail -s mail.internode.on.net -f jeremy.carver@.com -t jeremy.carver@.com
;;
*)
echo "Hmmm. I expected some kind of percentage"
esac
