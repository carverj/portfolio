### Hi there 👋 👨‍💻

<br/>

<p>
I am an IT Professional 🚀 from Australia. Always learning something new!

TODO: Repurposing the Landing Page:

Then the signed up clients can be referred here as a reference point for their project management and content creation needs!

<br/>

# Portfolio

## PROJECT: IBM Websphere Content Management System

Product is currently known as TinyMCE online editor: https://www.tiny.cloud/

A little bit of PHP work but they were mainly a Java shop! 

testing of IBM websphere integration tools and  content management systems. Includes importing assets such as content libraries

Built QA test environment including virtualized and dedicated machines and also variety different mobile devices

Automation - Docker, ansible, vagrant, IE, build security hardened centos 7 vagrant box using ansible playbook

           - Create LAMP wordpress server running as two processes/containers on engineers local machine

           - unattended installs of new machines using kickstart/preseed dished out from nginx webserver

Continuous integration - Create jobs to monitor build slaves in Jenkins

                       - use python, bash, jq, curl to manipulate JSON. Cron

                       - jenkins 2 build jobs

Web server/automation - use templates to build an Ubuntu Xenial 16 nginx/PHP server and configure with ansible playbook
                 
                      - Switched to new server in DNS, ssh key management for continuous integration

                      - nginx configuration 

                      - install latest java, work with engineers to debug and iron out apache2 to nginx PHP issues

Product deployments including upgrading IBM Websphere Application Server to latest and Java upgrades

Github account management including pushing repos to Bitbucket server/STASH

Manage infrastructure job board (resolved 363 of 366 tasks/issues/projects). Technically I was a product manager 

    - managing workflows and reporting using Atlassian Jira

Database administration of network monitoring servers - sqlite3, mysql, SQL (Stucture Query Language)

bash scripting, python, YAML, perl, ruby, powershell 5, powercli

               - create jenkins jobs that kick of shell scripts which backup critical data. Include sanity testing


## PROJECT: Python Heroku web product landing page and backend

A Python/Heroku/SCSS/AWS/Postgres app that was completely free to create. NOTE: until suddenly Heroku decided it was no longer free!

![An app that was free to create!](images/portfolio-deployed.png)

Static site deployed:- https://carverj.gitlab.io/portfolio/
Front end shown here: https://codepen.io/carverj/full/BEjvwx

Built with responsiveness in mind so it can be viewed neatly on a smartphone!

## Python Fibonacci example

Using binet's formula to improve on pythons sluggishness. Can be run with following command once execution permitted:


```
chmod +x fancy-fib.py
python3 fancy-fib.py
```

```
n = 1400
###################################
# 1. An iterative method
###################################

# Iterative method, with values saved in a list
#fiblist = [0,1]
#for i in range(n - 1):
#    fiblist.append(fiblist[i] + fiblist[i+1])
#print(fiblist)


###################################
# 2. A recursive method
###################################
#def fibRec(n):
#    if n < 2:
#        return n
#    else:
#        return fibRec(n-1) + fibRec(n-2)
#print([fibRec(i) for i in range(n + 1)])


###################################
# 3. Using Binet's formula
###################################
def fibBinet(n):
    phi = (1 + 5**0.5)/2.0
    return int(round((phi**n - (1-phi)**n) / 5**0.5))
print([fibBinet(i) for i in range(n + 1)])
```

```
Output: [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610    .......................
17108476902341031930900872229799311823548885207520250059879218397271294133609445289648054929224955689334680998073906028330271631735954078631102159069869493824679624667027869177183344323198587677678797743665546357671574787423957052761591662008848484182450619678732146911660459105375449243451392]
time: real	0m0.208s            <-- much faster!!
```

```
UPDATE: I achieved an even faster result when using functional programming in Python:-

def fibonacci(n, first=0, second=1):
    for _ in range(n):
        print(first)
        first, second = second, first + second
fibonacci(1400)

time: real	0m0.115s
```

## Bash script example

For example, 2, 3, 5, 7 and 11 are the first few prime numbers

```
chmod +x primes.sh
./primes.sh
```

```

#!/bin/bash
# primes between 10000 and 10500

declare -i firstnum=10000
declare -i lastnum=10500

# primelist will contain all prime values up to the square root of $1
primelist="2"

    echo "Positive primes up to $lastnum"
    if [ $lastnum -eq 10500 ]; then
     echo "10002"
     # Now only look at odd numbers greater than 2
     for n in `seq 10003 2  $lastnum`
     do
         # Flag this one as prime until proven otherwise
         p=0
         for t in $primelist
         do
             (( remainder = n%t ))
             echo "the remainder variable contains= $remainder"
             if [ $remainder -eq 0 ]; then
                 p=1
                 # Skip to next now we know not a prime
                 break
             fi
         done
         if [ $p -eq 0 ]; then
          # Found a prime
          echo $n
          if (( lastnum > (n * n) )) ; then
              primelist="$primelist $n"
              echo "primelist now contains $primelist"
          fi
         fi

     done
    fi
```
```
Output: Positive primes up to 10500
10002
the remainder variable contains= 1
10003
the remainder variable contains= 1
10005
the remainder variable contains= 1
10007
...
...
10493
the remainder variable contains= 1
10495
the remainder variable contains= 1
10497
the remainder variable contains= 1
1049
```
## Python script to power off AWS instances on schedule

```
# See: https://stackabuse.com/automating-aws-ec2-management-with-python-and-boto3/
# Example of stopping an instance. So we may run this task when we wish to stop an instance. we may use cron or task scheduler to stop instance at COB
# awsutils

import boto3

def get_session(region):
    return boto3.session.Session(region_name=region)

import awsutils
session = awsutils.get_session('us-east-1')
client = session.client('ec2')

import pprint
pprint.pprint(client.describe_instances())

demo = client.describe_instances(Filters=[{'Name': 'tag:Name', 'Values': ['demo-instance']}])
pprint.pprint(demo)

instance_id = demo['Reservations'][0]['Instances'][0]['InstanceId']

pprint.pprint(client.stop_instances(InstanceIds=[instance_id]))
{'ResponseMetadata': {'HTTPHeaders': {'content-length': '579',
                                      'content-type': 'text/xml;charset=UTF-8',
                                      'date': 'Sat, 22 Dec 2018 19:26:30 GMT',
                                      'server': 'AmazonEC2'},
                      'HTTPStatusCode': 200,
                      'RequestId': 'e04a4a64-74e4-442f-8293-261f2ca9433d',
                      'RetryAttempts': 0},
 'StoppingInstances': [{'CurrentState': {'Code': 64, 'Name': 'stopping'},                                 <--
                        'InstanceId': 'i-0c462c48bc396bdbb',
                        'PreviousState': {'Code': 16, 'Name': 'running'}}]

# can check that it has stopped
instance.state
```
## Creating wordpress docker container

Instructions are based on the blog https://www.sitepoint.com/how-to-manually-build-docker-containers-for-wordpress/. Docker will automatically download the necessary packages if it detects that you don't have them installed on your PC.
Install docker

    Go to http://www.docker.com
    Download and run the installer.
    Start up Docker in the background.

Create a MySQL Container

    Open a command prompt / terminal window and run:
    > docker run --name wordpressdb -e MYSQL_ROOT_PASSWORD=password -d mysql
        the name can be changed but make sure you enter the correct name when starting the Wordpress container.
        the password can be changed, but again make sure it matches when starting the Wordpress container.
    If this is the first time, wait for the download process to occur and the container should start.
    Run the following command to confirm the container is running:
    > docker ps

Creating a Wordpress Container

    Open a command prompt / terminal window and run:
    > docker run -e WORDPRESS_DB_PASSWORD=password -d --name wordpress --link wordpressdb:mysql -p 8080:80
        the name can be changed.
        the link must match the MySQL container name.
        the password must match the MySQL Root Password set earlier.
        the port can be changed, eg to set it to port 1234 use "-p 1234:80"
    If this is the first time, wait for the download process to occur and the container should start.
    Run the following command to confirm the container is running:
    > docker ps

Configuring Wordpress

    Navigate to http://localhost:8080
    Follow the prompts to set up the Wordpress installation.

Stopping the Containers

    Open a command prompt / terminal window and run the command to find all running containers:
    > docker ps
    For each of the containers run the stop command:
    > docker stop wordpress
    > docker stop wordpressdb

Starting the Containers

    Open a command prompt / terminal window and run the command to check if the container is running:
    > docker ps
    For each of the containers run the start command:
    > docker start wordpressdb
    > docker start wordpress

## Adobe Photoshop versus GIMP

![Creating a mock poster in GIMP](images/KevinCookie.png)

Creating a mock poster using free and open source GIMP program

![GIMP screenshot showing completed poster](images/Screenshot_2023-07-11_17-23-15.png)

GIMP screenshot showing completed poster

Credit: Thanks to Kevin Stratvert and his GIMP tutorial on youtube.com   
# 





