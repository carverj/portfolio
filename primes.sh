#!/bin/bash
# primes between 10000 and 10500

declare -i firstnum=10000
declare -i lastnum=10500

# primelist will contain all prime values up to the square root of $1
primelist="2"

    echo "Positive primes up to $lastnum"
    if [ $lastnum -eq 10500 ]; then
     echo "10002"
     # Now only look at odd numbers greater than 2
     for n in `seq 10003 2  $lastnum`
     do
         # Flag this one as prime until proven otherwise
         p=0
         for t in $primelist
         do
             (( remainder = n%t ))
             echo "the remainder variable contains= $remainder"
             if [ $remainder -eq 0 ]; then
                 p=1
                 # Skip to next now we know not a prime
                 break
             fi
         done
         if [ $p -eq 0 ]; then
          # Found a prime
          echo $n
          if (( lastnum > (n * n) )) ; then
              primelist="$primelist $n"
              echo "primelist now contains $primelist"
          fi
         fi

     done
    fi
